#! /usr/bin/env ruby
# frozen_string_literal: true

if ARGV.empty? or ARGV[0] !~ /^streamwatcher:\/\//
  exit 1
end

_matchdata = /^streamwatcher:\/\/(\w+)\/(\w+)\/?(\?[\w&=,]+)?/.match(ARGV[0])
if !_matchdata
  warn "Failed to parse URI"
  exit 1
end

domain = _matchdata[1]
target = _matchdata[2]
parameters = _matchdata[3]

case domain
when "twitch"
  # URI represents a stream
  quality = ""
  chat = ""
  video = ""
  if parameters
    parameters.match /(?<=(?:&|\?)quality=)[\w,]+/ do |m|
      quality = m[0]
    end
    parameters.match /(?<=&|\?)chat(?:=(\w+))?/ do |m|
      if !m[1] or m[1] == 'true'
        chat = 'chat'
      elsif m[1] == 'false'
        chat = 'no-chat'
      end
    end
    parameters.match /(?<=&|\?)video(?:=(\w+))?/ do |m|
      if !m[1] or m[1] == 'true'
        video = 'video'
      elsif m[1] == 'false'
        video = 'no-video'
      end
    end
  end
  spawn("streamwatcher-launch.rb", target, quality, chat, video)

when "signal"
  # URI is for signaling a process
  signal = nil
  pid = target.to_i
  exit 2 if pid.zero?
  if parameters
    parameters.match /(?<=(?:&|\?)signal=)(?:SIG)?\w+/i do |m|
      if m =~ /^\d+$/
        sig = m.to_i
        signal = sig if Signal.signame(sig)
      else
        sig = m.to_s.upcase
        signal = sig if Signal.list[sig]
      end
    end
  end
  unless signal.nil?
    Process.kill(signal, pid)
  else
    exit 2
  end
end
