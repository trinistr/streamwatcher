#! /usr/bin/env ruby
# frozen_string_literal: true

# ---- CONFIGURATION ----

# Path to a file where channel list can be found: first field on every line
# is a channel name, every field after the first is considered an alias.
# All names (and aliases) will be matched by prefix, for example
# "esa" on command line will match "esamarathon" in channel list.
# Everything on the line after a '#' character will be considered a comment.
# Set to nil or empty string to disable aliases and prefix matching.
Channels = '~/twitchstreams'

# Whether to open video by default.
DefaultVideo = true
# Default quality string to pass to the livestream program.
DefaultVideoQuality = '720p,720p60,best'
# Command line to launch livestream. Parameters: channel name, quality.
VideoCommand = 'streamlink https://twitch.tv/%1$s "%2$s"'

# Whether to open chat by default.
DefaultChat = false
# Command line to launch chat. Parameters: channel name.
ChatCommand = 'java -jar ~/bin/Chatty.jar -connect -channel "%1$s" >/dev/null 2>/dev/null'

# Whether to wait on child processes, or exit immediately
WaitForExit = {
  chat: false,
  video: false
}



# ---- NO CONFIGURATION BELOW ----

program_name = File.basename($0, '.*')

if ARGV.empty? or ARGV.include? '-h' or ARGV.include? '--help'
  puts "Usage: #{program_name} <channel> [<quality>] [chat|no-chat] [video|no-video]"
  exit
end

channel = ARGV.shift
quality = DefaultVideoQuality
chat = DefaultChat
video = DefaultVideo
while arg = ARGV.shift
  case arg
  when /^c/
    chat = true
  when /^(?:no-c|noc|nc)/
    chat = false
  when /^v/
    video = true
  when /^(?:no-v|nov|nv)/
    video = false
  else
    quality = arg unless arg.match(/^\s*$/)
  end
end

if Channels && Channels != "" && File.readable?(File.expand_path(Channels))
  File.open(File.expand_path(Channels)) do |file|
    found_channel = file.grep(/^[^#]*\b#{Regexp.escape(channel)}/i)[0]
    if found_channel
      channel = found_channel.slice(/^\w+/)
      puts "#{program_name}: found matching alias for channel '#{channel}'"
    else
      puts "#{program_name}: opening by provided name"
    end
  end
end

if chat
  puts "#{program_name}: launching chat for '#{channel}'"
  chatpid = spawn(format(ChatCommand, channel))
end
if video
  puts "#{program_name}: launching stream '#{channel}'"
  videopid = spawn(format(VideoCommand, channel, quality))
end

if WaitForExit[:video]
  Process.wait videopid unless videopid.nil?
end
if WaitForExit[:chat]
  Process.wait chatpid unless chatpid.nil?
end
