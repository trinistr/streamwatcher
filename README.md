# Streamwatcher

Streamwatcher is a set of CLI utilities supporting watching streams on Twitch.

The goal is to provide useful lightweight stand-alone programs for stream
tracking and launching preferred programs for watching them, while abstaining from
"helpfully" creating hidden files. All of the configuration can be done in
the programs themselves.

## Requirements

Streamwatcher requires a working Ruby interpreter. It is tested on version
2.7, but should work with 2.5 and possibly with 2.3.

There are additional requirements for some `streamwatcher-check`'s functions:
- notitfications require `notify-send` program to be installed.
- hyperlinking requires specific support from the terminal emulator, this is not
  the more common URL support; look at [this list][hyperlink support] to learn if
  yours supports hyperlinks.

## Installing

Depending on your needs (see Recipes section), you will have to do some or all of the next:
1. Copy Ruby scripts to a directory in your PATH, such as `~/.local/bin`.
2. Copy .desktop file to an applications directory, such as `~/.local/share/applications`.
3. Add association for `x-scheme-handler/streamwatcher`:
  - The simplest way is to open `~/.config/mimeapps.list` and, under
    [Default Applications] header, add
    ```
    x-scheme-handler/streamwatcher=streamwatcher-uri.desktop
    ```
  - Another way is to use xdg-mime or gio:
    ```
    xdg-mime default streamwatcher-uri.desktop x-scheme-handler/streamwatcher
    ```
    ```
    gio mime x-scheme-handler/streamwatcher streamwatcher-uri.desktop
    ```
    These programs basically do the same thing.
  - See [freedesktop.org mime association specification][] for other
    possible places to put the association.
4. Run `streamwatcher-check.rb --get-token` and follow the URL to get the
   token for accessing the Twitch API. Put this token in `streamwatcher-check`'s
   configuration.

## Configuration

Configuration for `streamwatcher-check` and `streamwatcher-launch`
is done in the scripts themselves at the top. Read the comments to learn that
everything does.

- `streamwatcher-check` supports putting channel list and token in external files.
  This is recommended to reduce changes to the script's configuration. These files
  can be shared with other programs.
  - Channel list file must have one channel name per line, everything after that
    in a line gets ignored. This file is intended to be shared
    with `streamwatcher-launch`.
  - Token file must have something like `token=AbCd1234` anywhere in it. This
    file in particular can be shared with other programs and parsing is quite
    lenient to support this.

- `streamwatcher-launch` supports using an external channel list file, probably
  shared with `streamwatcher-check`, to provide aliases for channel names.
  Every line in this file must have the real channel name as first field, and
  every field after specifies an alias. Case is ignored when searching for aliases.
  Example:
  ```
  GamesDoneQuick gdq
  RPGLimitBreak rpglb LimitBreak
  ```

The `streamwatcher-uri-handler` script has no configuration.

## Recipes

Streamwatcher is intended to be used alongside other applications. This means
that, depending on your specific case, different parts of the suite can be used.
This section provides recipes for setting up in different cases.

**A. I watch streams in my browser, but want to track them with Streamwatcher.**

- Do steps 1 and 4 from Installation section (only `streamwatcher-check.rb`
  needs to be copied).
- Configure `streamwatcher-check` to your liking. Don't forget to provide a list
  of channels to track.
- If your terminal does not support hyperlinks or you don't want to use them,
  disable them in Printing > hyperlinks > enabled. Otherwise:
  - Find Printing > hyperlinks > uri and replace the URI there with
    "https://twitch.tv/%s".
  - Go to the next item, alternatives, and delete everything between square brackets.
- Run `streamwatcher-check.rb` in your terminal. You should see current time and
  a list of online streams.

**B. I track streams with some other application, but want to quickly open them in browser.**

- Do step 1 from Installation section (only `streamwatcher-launch.rb` needs to
  be copied).
- Configure `streamwatcher-launch`:
  - Set Channels to a file with your aliases, if you intend to use them.
  - Set VideoCommand to "\<browser\> https://twitch.tv/%1$s", where \<browser\> is
    the program name of your browser (usually x-www-browser can be used).
- Run `streamwatcher-launch.rb <channel>`, where \<channel\> is a channel name
  or configured alias. This should start the browser at the correct page.

**C. I track streams with some other application, but want to quickly open them in streamlink and Chatty.**

- Do step 1 from Installation section (only `streamwatcher-launch.rb` needs to
  be copied).
- Configure `streamwatcher-launch`:
  - Set Channels to a file with your aliases, if you intend to use them.
  - Set DefaultVideoQuality to a list of preferred stream qualities
    (see `streamlink` documentation).
  - Put correct path to Chatty.jar into ChatCommand.
  - Set DefaultChat to true if you want it open by default.
- Run `streamwatcher-launch.rb <channel>`, where \<channel\> is a channel name
  or configured alias. This should start streamlink and Chatty (if enabled by default).
- You can also select different quality and enable/disable chat or video:
  - Pass a quality list to use it instead of the configured one.
  - Add "chat", "no-chat", "video", or "no-video" as required.
  - Example: `streamwatcher-launch.rb GDQ 1080p no-chat`

This recipe can of course be adapted for other software with sane command-line parameters.

**D. I want to use Streamwatcher to track streams and quickly start them.**

If your terminal does not support hyperlinks:
- Follow recipe A.
- Follow recipe B or C, as appropriate.

If your terminal supports hyperlinks:
- Do steps 1-3 from Installation section.
- Do step 4 if you don't have a token from other application, such as streamlink,
  or if you want a separate one for Streamwatcher.
  Technically, you shouldn't reuse tokens between applications, but realistically
  it doesn't matter much. Streamwatcher doesn't use any permissions when accessing
  Twitch API, so absolutely any token will do.
- Configure `streamwatcher-check` to your liking. Don't forget to provide a list
  of channels to track.
- Configure `streamwatcher-launch` (see recipe B or C as appropriate).
- Run `streamwatcher-check.rb` in your terminal. You should see current time and
  a list of online streams.
- Ctrl+click on a channel name. This should start the stream in the program
  configured in `streamwatcher-launch`.

## Copyright notice

Copyright © 2019,2020 Aleksandr Bulantcov

This software is licensed under the terms of Mozilla Public License 2.0.
See LICENSE file for more information.

[hyperlink support]: https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda
[freedesktop.org mime association specification]: https://specifications.freedesktop.org/mime-apps-spec/latest/index.html
