#! /usr/bin/env ruby
# frozen_string_literal: true

# ---- CONFIGURATION ----

# Channels to track. Can be a file or an array:
# 1) path to a file where channel names can be found; one name per line,
# everything after the first field is ignored.
# 2) array of channel names, for example ["RiffTrax", "SaltyBet", "Speedrun"]
# In either case capitalisation is not significant.
# If using a file, channels can be reloaded at runtime, either automatically or
# on signal (see Runtime below).
Channels = "~/twitchstreams"
# Authentication token. Can be a file or token itself:
# 1) if file == true, token is path to file with a string of the form
# token = <token>, where <token> is the real token. The search is done in a
# fuzzy fashion, so using other programs' configuration is possible.
# For example, streamlink and Chatty configuration can be used.
# This is the recommended way to store the token for security reasons.
# 2) if file == false, token is the token itself.
# If using a file, token can be reloaded at runtime, either automatically or
# on signal (see Runtime below).
Token = {
	file: true,
	token: "~/.streamlinkrc"
}
# Various settings for channel status updates.
Updates = {
	# Time between updates (in seconds).
	interval: 60,
	# Time between updates when detecting no network connection (in seconds).
	interval_no_network: 30,
	# Whether to delay channel going offline. This solves intermittent problems
	# with channel being offline when it is not, but causes issues when a lot of
	# time passes between checks (not recommended).
	delay_going_offline: false,
	# Which Twtich API version to use. Allowed values:
	# :kraken_v5 (recommended), :helix (new).
	api_version: :helix,
}
# Settings for printing the status of channels.
Printing = {
	# Whether to display stream time and in which way: either :since, :for or nil.
	# :since displays the stream start time, :for displays the run time, and
	# nil disables the display.
	time_display: :for,
	# Delay between displaying channels (in seconds). Useful when immediate
	# printing is not aesthethically pleasing. Good values are around 0.02.
	delay: 0,
	# Hyperlinking in update text. Makes channel names hyperlinks to easily open
	# the streams. Hyperlinks require special support from the terminal.
	hyperlinks: {
		enabled: true,
		# URI fromat string to open when pressing the channel name.
		# Parameters are: name.
		uri: "streamwatcher://twitch/%s?video=true&chat=true",
		# Alternative launch links which are displayed under channel name.
		# Parameters are: name.
		alternatives: [
			{
				# Video only
				uri: "streamwatcher://twitch/%s?chat=false",
				text: "v",
			},
			{
				# Best video quality
				uri: "streamwatcher://twitch/%s?quality=best",
				text: "b",
			},
			{
				# Audio only
				uri: "streamwatcher://twitch/%s?quality=audio_only",
				text: "a",
			},
			{
				# Chat only
				uri: "streamwatcher://twitch/%s?chat=true&video=false",
				text: "c",
			}
		]
	},
	# Filter out results that match any of the patterns.
	# Patterns are regular expressions, but basically can be a substring.
	# Example: "World of Warcraft" in game: will filter out all versions of WoW.
	filters: {
		name: [],
		game: [],
		title: [],
	},
}
# Sending channel status change notifications via notify-send.
Notifications = {
	enabled: false,
	# Set which events trigger notifications.
	events: {
		online: true, offline: true, game: true, title: true
	},
	# Which icons to use for different events; can be empty strings.
	# Icons here can be full paths or icon names from current icon theme.
	icons: {
		online: "face-surprise", offline: "face-sad",
		game: "face-uncertain", title: "face-uncertain"
	},
	# Urgency level. Allowed values: low, normal, critical.
	urgency: "low",
	# Format strings for notifications. Parameters are: name, game, title.
	text: {
		summary: {
			online: "%1$s is now online!",
			offline: "%1$s went offline",
			game: "%1$s changed game!",
			title: "%1$s changed title!"
		},
		body: {
			online: "Playing <b>%2$s</b>\n«%3$s»",
			offline: "",
			game: "Playing <b>%2$s</b>\n«%3$s»",
			title: "Playing <b>%2$s</b>\n«%3$s»"
		}
	},
}
# Logging statistical data in CSV format.
Statistics = {
	enabled: false,
	# File to write statistics to.
	file: "~/stat_twitch",
	# Which events to log.
	events: {online: true, offline: false, game: false, title: false},
	# Which fields to put into log.
	# Allowed values: :name, :game, :title, :time, :action.
	fields: [:name, :time, :action, :game],
	# Field separator.
	separator: ";",
}
# Reloading configuration and immediately updating at runtime.
Runtime = {
	enabled: true,
	# Reload configuration on signal.
	# Only channels list and token from external files can be reloaded.
	reload_on_signal: {
		# Signal to trigger; can be nil to disable.
		signame: "USR1",
		# What to reload.
		what: [:channels, :token],
		# Whether to announce changes in terminal.
		verbose: true,
		# Whether to update immediately afterwards.
		update: true,
	},
	# Reload configuration on file modification.
	# Only channels list and token from external files can be reloaded.
	# Changes will usually be visible on next update.
	reload_on_modification: {
		# What to reload.
		what: [:channels, :token],
		# Whether to announce changes in terminal.
		verbose: true,
	},
	# Update immediately on signal, especially useful with hyperlinking.
	update: {
		# Signal to trigger; can be nil to disable.
		signame: "USR2",
		# Hyperlink text (if hyperlinks are enabled); set to nil to disable.
		hyperlink: "🔃",
	}
}

# ---- NO CONFIGURATION BELOW ----

require 'json' # Parsing Twitch API data
require 'open-uri' # Easy API interaction
require 'io/console' # Console width
require 'time' # For parsing time string. Could be done with Time.strptime, but this is easier
require 'socket' # To make SocketError defined
require 'net/http' # To make Net defined

Application = {
	id: 'ob7ez3qnbvb5372infqeb7eqna4a3t',
	redirect_uri: 'https://twitchapps.com/tokengen/',
	scope: '',
}

# ---- Generic stuff (does not depend on configuration)
# Channels are considered equal if they have the same name (eql, ==, hash)
class Channel
	attr_reader :name, :display_name, :game, :title, :stream_type, :went_online, :went_offline, :data
	def initialize(name)
		@name = name.downcase
		@display_name = name
		@online = false
	end

	def go(data, delayed: false)
		if data[:online]
			oldgame = @game
			oldtitle = @title
			@pending_offline = false
			@stream_type = data[:type]
			@game = data[:game]
			@title = data[:title]
			@display_name = data[:display_name] if data[:display_name]
			@data = data
			if !@online
				@online = true
				@went_online = Time.parse(data[:started_at])
				if block_given?
					yield ({name: @name, time: @went_online, action: :online,
							game: @game, title: @title})
				end
			else
				if block_given?
					if @game != oldgame
						yield ({name: @name, time: Time::now, action: :game,
								game: @game, title: @title})
					elsif @title != oldtitle
						yield ({name: @name, time: Time::now, action: :title,
								game: @game, title: @title})
					end
				end
			end
		elsif !delayed || @pending_offline
			if !delayed
				@went_offline = Time::now
			else
				@went_offline = @pending_went_offline
			end
			if block_given? && @online
				yield ({name: @name, time: @went_offline, action: :offline})
			end
			@online = false
			@game = @title = @stream_type = nil
		elsif !@pending_offline && @online
			@pending_offline = true
			@pending_went_offline = Time::now
		end

	end

	def online?
		return @online
	end

	def ==(other)
		if self.equal? other
			return true
		elsif other.is_a? Channel
			return @name == other.name
		else
			return false
		end
	end
	alias eql? ==

	def hash
		return @name.hash
	end
end

class TwitchAPI
	NETWORK_EXCEPTIONS = [
		OpenURI::HTTPError, ::SocketError, Errno::ECONNRESET, Net::OpenTimeout
	]

	class << self
		def check_token(token)
			# returns [valid, username], where username may not be present
			check_token_impl(token)
		end

		def maximum_channels_per_query()
			@maximum_channels_per_query
		end

		def query_all_streams(channels, token)
			ret = []
			threads = []
			for i in 0.step(channels.length - 1, @maximum_channels_per_query)
				threads << query_streams(channels[i...(i + @maximum_channels_per_query)], token)
			end
			threads.each do |t|
				t.join
				t.value.each {|v| ret << v }
			end
			return ret
		end

		private

		def query_streams(channels, token)
			# returns a Thread, its value is an Array with following Hash for each channel:
			# { name:, online:, game:, title:, viewer_count:, started_at:, type:, thumbnail_url: }
			Thread.new do
				Thread.current.report_on_exception = false
				begin
					tries ||= 0
					query_streams_impl(channels, token)
				rescue
					if (tries += 1) < 3
						sleep tries * 0.5
						retry
					else
						raise
					end
				end
			end
		end
	end
end

class TwitchAPIv5 < TwitchAPI
	@maximum_channels_per_query = 100
	@user_ids = {}
	class << self
		def check_token_impl(token)
			ret = []
			URI.open(
					"https://api.twitch.tv/kraken",
					"Accept" => "application/vnd.twitchtv.v5+json",
					"Authorization" => "OAuth #{token}"
			) do |f|
				data = JSON.parse(f.read)
				if data["token"] && data["token"]["valid"]
					ret << true
					ret << data["token"]["user_name"]
				else
					ret << false
				end
			end
			return ret
		end

		def query_streams_impl(channels, token)
			channels_without_ids = channels.select {|ch| !@user_ids.has_key? ch.name }
			                               .map {|ch| ch.name }
			unless channels_without_ids.empty?
				URI.open(
						"https://api.twitch.tv/kraken/users?login=#{channels_without_ids.join(',')}",
						"Accept" => "application/vnd.twitchtv.v5+json",
						"Authorization" => "OAuth #{token}"
				) do |f|
					data = JSON.parse(f.read)
					found_names = data["users"].map {|user| user["name"] }
					not_found = channels_without_ids - found_names
					unless not_found.empty?
						warn "Following channels have not been found: #{not_found.join(', ')}"
					end
					data["users"].each {|user| @user_ids[user["name"]] = user["_id"] }
					not_found.each {|user| @user_ids[user] = nil }
				end
			end
			ret = []
			online = {}
			ids = channels.map {|ch| @user_ids[ch.name] }
			              .reject {|ch| ch == nil }
			              .join ","
			URI.open(
					"https://api.twitch.tv/kraken/streams/?limit=#{@maximum_channels_per_query}&stream_type=all&channel=#{ids}",
					"Accept" => "application/vnd.twitchtv.v5+json",
					"Authorization" => "OAuth #{token}"
			) do |f|
				data = JSON.parse(f.read)
				if data["streams"]
					data["streams"].each do |stream|
						hash = { name: stream["channel"]["name"], online: true }
						hash[:display_name] = stream["channel"]["display_name"]
						hash[:game] = stream["game"]
						hash[:started_at] = stream["created_at"]
						hash[:type] = if stream["is_playlist"]
								"playlist"
							else
								stream["stream_type"]
							end
						hash[:title] = stream["channel"]["status"]
						hash[:viewer_count] = stream["viewers"]
						hash[:thumbnail_url] = stream["preview"]["template"]
						ret << hash
						online[hash[:name]] = true
					end
				end
			end
			channels.reject {|ch| online[ch.name] }
			        .each {|ch| ret << {name: ch.name, online: false} }
			return ret
		end
	end
end

class TwitchAPIHelix < TwitchAPI
	@maximum_channels_per_query = 100
	@game_ids = {}
	@client_id = nil
	class << self
		def check_token_impl(token)
			ret = []
			URI.open(
					"https://id.twitch.tv/oauth2/validate",
					"Authorization" => "OAuth #{token}"
			) do |f|
				data = JSON.parse(f.read)
				if data["login"] && data["expires_in"] > 0
					ret << true
					ret << data["login"]
					@client_id = data["client_id"]
				else
					ret << false
				end
			end
			return ret
		end

		def query_streams_impl(channels, token)
			ret = []
			channelstring = channels.map {|ch| "user_login=" + ch.name} .join "&"
			games_without_names = []
			online = {}
			URI.open(
					"https://api.twitch.tv/helix/streams?first=100&#{channelstring}",
					"Authorization" => "Bearer #{token}",
					"Client-Id" => "#{Application[:id]}"
			) do |f|
				data = JSON.parse(f.read)
				data["data"].each do |stream|
					hash = {
						display_name: stream["user_name"],
						name: stream["user_name"].downcase,
						online: true,
						type: stream["type"],
						title: stream["title"],
						viewer_count: stream["viewer_count"],
						thumbnail_url: stream["thumbnail_url"],
						started_at: stream["started_at"],
						game: stream["game_id"]
					}
					games_without_names << hash[:game] if !@game_ids.has_key? hash[:game]
					online[hash[:name]] = true
					ret << hash
				end
			end
			unless games_without_names.empty?
				gamestring = games_without_names.map {|g| 'id=' + g } .join '&'
				URI.open(
					"https://api.twitch.tv/helix/games?#{gamestring}",
					"Authorization" => "Bearer #{token}",
					"Client-Id" => "#{Application[:id]}"
				) do |f|
					data = JSON.parse(f.read)
					data["data"].each {|game| @game_ids[game["id"]] = game["name"] }
				end
			end
			ret.each {|stream| stream[:game] = @game_ids[stream[:game]] }
			channels.reject {|ch| online[ch.name] }
			        .each {|ch| ret << {name: ch.name, online: false} }
			return ret
		end
	end
end

class Mutex
	def enter
		raise ThreadError, "trying to enter mutex without block" if !block_given?
		need_lock = !owned?
		lock if need_lock
		yield
		unlock if need_lock
	end
end

# ---- Non-generic stuff

UpdateMutex = Mutex.new

Printer = Object.new
class << Printer
	def print(channels)
		max_channel_name_length = channels.inject(8) {|m, c|
				c.display_name.length > m ? c.display_name.length : m }
		windowWidth = IO.console.winsize[1]
		titleWidth = windowWidth - (max_channel_name_length + 3)

		printer_lock = !UpdateMutex.owned?
		UpdateMutex.lock if printer_lock  # Can be already locked in main loop

		puts
		Kernel.print Time::now
		print_update_link
		puts

		channels.select {|channel| channel.online?}.each {|channel|
			next if Printing[:filters].select {|_t, f| f && !f.empty? } .any? do |type, filter|
				filter.any? {|expr| channel.data[type].match? expr }
			end
			if IO.console
				if Printing[:hyperlinks][:alternatives].is_a? Array and !Printing[:hyperlinks][:alternatives].empty?
					alternatives = ' ' + (Printing[:hyperlinks][:alternatives].map {|a|
						osc8_hyperlink(a[:uri] % channel.name, a[:text] % channel.display_name) } .join ' ')
					alternatives_length = 1 + (Printing[:hyperlinks][:alternatives].map {|a|
						a[:text] % channel.display_name } .join(' ').length)
				end
				printf "%*s%s %c %s\n",
						max_channel_name_length - channel.display_name.length, "",
						hyperlink(channel.display_name),
						(channel.stream_type != "live" ? "↻" : "–"),
						channel.game
				title_lines = columnise('«' + channel.title.strip + '»', titleWidth)
				stream_time =
					if Printing[:time_display] == :for
						Time.at(Time.now - channel.went_online).utc.strftime('%H:%M')
					elsif Printing[:time_display] == :since
						if Time.now - channel.went_online < 24*3600
							channel.went_online.strftime('%H:%M')
						else
							channel.went_online.strftime('%a %H:%M')
						end
					end
				printf "%*s%s%s   %s\n",
					max_channel_name_length - (stream_time.length + alternatives_length.to_i), '',
					stream_time, alternatives.to_s, title_lines[0]
				title_lines[1..-1].each {|line|
					printf "%*s   %s\n", max_channel_name_length, nil, line
				}
				sleep Printing[:delay]
			else
				printf "%*s %c %s «%s»\n",
					max_channel_name_length, channel.display_name,
					(channel.stream_type != "live" ? "↻" : "–"),
					channel.game,
					channel.title
			end
		}
		UpdateMutex.unlock if printer_lock
	end

	# Splits text to a specified width
	def columnise(text, width)
		text_lines = text.split("\n")
		lines = []
		text_lines.each {|text_line|
			while text_line.length > width
				space_index = text_line.rindex(/\s/, width - 1)
				if space_index
					lines << text_line.slice!(0..space_index).chop
				else
					lines << text_line.slice!(0...width)
				end
			end
			lines << text_line unless text_line.empty?
		}
		lines
	end

	def osc8_hyperlink(link, text)
		return "\e]8;;#{link}\e\\#{text}\e]8;;\e\\"
	end

	def hyperlink(text)
		if Printing[:hyperlinks][:enabled]
			link = Printing[:hyperlinks][:uri] % [text, Printing[:hyperlinks][:quality], Printing[:hyperlinks][:chat] ? "chat" : nil]
			return osc8_hyperlink link, text
		else
			return text
		end
	end

	def print_update_link
		if Printing[:hyperlinks][:enabled] and Runtime[:enabled] and Runtime[:update][:hyperlink] and IO.console
			Kernel.print ' ', osc8_hyperlink(
				"streamwatcher://signal/#{Process.pid}?signal=#{Runtime[:update][:signame]}",
				Runtime[:update][:hyperlink])
		end
	end

	def print_array(array)
		puts "<nothing>" if array.empty?
		text = array.join(", ")
		columnise(text, IO.console.winsize[1]).each {|line| puts line}
	end
end

# Fat singleton for runtime data
P = Object.new
class << P
	attr_reader :channels, :channels_mtime, :channels_path,
			:token, :token_mtime, :token_path,
			:statFile, :username, :api

	def load_token(token, force = false)
		if token[:file]
			@token_path ||= File.expand_path(token[:token])
			loaded_token = nil
			begin
				if force || !@token_mtime || File.mtime(@token_path) > @token_mtime
					File.open(@token_path) do |file|
						begin
							loaded_token = file.grep(/token/)[0].match(/token\W+([a-z0-9]+)/)[1]
							@token_mtime = file.mtime
						rescue
							warn "File '#{Token[:token]}' does not contain a token"
							warn "(must have a line with 'token')"
						end
					end
				else
					return :nop
				end
			rescue
				warn "Error while opening token file '#{Token[:token]}'"
				exit 3
			end
		else
			loaded_token = token[:token]
		end
		return nil if !loaded_token or loaded_token.empty?
		loaded_token
	end

	def check_token(token, force = false)
		return token if token.nil? || token == :nop
		token_valid, username = *@api.check_token(token)
		if token_valid
			UpdateMutex.enter {
				@token = token
			}
			@username = username
			return true
		else
			warn "Token invalid"
			return false
		end
	end

	def load_and_check_token(token, force = false)
		check_token(load_token(token, force), force)
	end

	def load_channels(channels, force = false)
		@channels ||= {}
			if channels.is_a? String
				@channels_path = File.expand_path(channels) if !@channels_path
				loaded_channels = {}
				begin
					if force || !@channels_mtime || File.mtime(@channels_path) > @channels_mtime
						File.open(@channels_path) do |file|
								file.each_line do |line|
									next if line =~ /^\s*$|^\s*#/
									name = line.slice(/\w+/).freeze
									loaded_channels[name.downcase.freeze] = Channel.new(name)
								end
								@channels_mtime = file.mtime
						end
					else
						return [:nop, nil]
					end
				rescue
					warn "Error while opening channels file '#{channels}'"
					exit 3
				end
				if loaded_channels.empty?
					warn "File '#{channels}' is empty"
					exit 2 if @channels.empty?
					return nil
					# There is no point in deleting every channel,
					# what would be the use?
				end
				if @channels.empty? then
					return @channels = loaded_channels
				else
					removed_channels = @channels.select {|k| !loaded_channels.has_key? k }
					new_channels = loaded_channels.select {|k| !@channels.has_key? k }
					removed_channels.each {|k,_v| @channels.delete k }
					new_channels.each {|k,v| @channels[k] = v }
					return [new_channels.values, removed_channels.values]
				end
			elsif channels.is_a? Array && (!@channels or @channels.empty?)
				channels.each do |name|
					name.freeze
					@channels[name.downcase.freeze] = Channel.new(name)
				end
				return [channels, nil]
			else
				warn "channels are neither a String or an Array"
				exit 3
			end
	end

	def open_statistics_file(statisticsFile, force = false)
			if statiticsFile && !@statFile && !force
				begin
					file = File.open (File.expand_path statisticsFile), "at"
					@statFile.close if @statFile
					@statFile = file
				rescue
					warn "Error while opening statistics file '#{statisticsFile}'"
					exit 3
				end
			end
	end

	def request_token
		link = "https://id.twitch.tv/oauth2/authorize?client_id=#{Application[:id]}&redirect_uri=#{Application[:redirect_uri]}&response_type=token&scope=#{Application[:scope]}"
		puts
		puts link
		puts
		puts "Authentication token for this application does not request any special permissions, but you should not share it anyway."
	end

	def start(api, token, channels, statistics)
		@api = api
		threads = []
		threads << Thread.new {
			Thread.current.report_on_exception = false
			load_and_check_token(token, true)
		}
		threads << Thread.new { load_channels(channels) }
		threads << Thread.new { open_statistics_file(statistics[:file]) } if statistics[:enabled]
		threads.each { |t| t.join }
		if threads[0].value == nil
			puts "Authentication token was not provided."
			puts "If this is your first time using this application, use this link to get one:"
			request_token()
			exit 1
		end
		if threads[0].value == false
			puts "Follow this link to get a new token:"
			request_token()
			exit 2
		end
		print "Will write statistics to '#{statisticsFile}'\n\n" if statistics[:enabled]
		print "Logging in as: #{@username}\n\n"
		puts "Loaded #{@channels.length} channels:"
		Printer.print_array(@channels.values.map {|ch| ch.display_name })
		puts
	end

	def sleep_waiting_for_connection(cause = nil)
		print "Can't reach Twitch API"
		print " (#{cause})" if cause
		print ", waiting #{Updates[:interval_no_network]} seconds"
		Printer.print_update_link
		puts
		sleep Updates[:interval_no_network]
	end
end

class SuspendedExecutionError < StandardError; end

# ---- Initialization
# Signal handling
Signal.trap("EXIT") {
	P.statFile.close if P.statFile
	exit
}
Signal.trap("INT", "EXIT")
Signal.trap("TERM", "EXIT")

API = case Updates[:api_version]
	when :kraken_v5 then TwitchAPIv5
	when :helix then TwitchAPIHelix
	end

# Handle special arguments
unless ARGV.empty?
	case ARGV[0]
	when "--get-token" then
		puts "Go to this link to get a token:"
		P.request_token()
		exit 0
	end
end

begin
	P.start(API, Token, Channels, Statistics)
rescue *TwitchAPI::NETWORK_EXCEPTIONS => e
	begin
		P.sleep_waiting_for_connection(e)
	rescue SignalException
	end
	retry
end

# Random stuff
P.statFile.puts if Statistics[:enabled]

# ---- Main loop
LoopThread = Thread.new do
loop do
	startTime = nil
	begin
		startTime = Time::now
		Thread.main.wakeup # Check for file modification
		UpdateMutex.lock unless UpdateMutex.owned?
		streams = API.query_all_streams(P.channels.values, P.token)
		streams.each do |stream|
			P.channels[stream[:name]].go(stream) do |stat|
				event = stat[:action]
				if Notifications[:enabled] && Notifications[:events][event]
					system("notify-send --urgency=#{Notifications[:urgency]} \
						--icon=#{Notifications[:icons][event]} \
						\"#{Notifications[:text][:summary][event] %
							[channel.name, channel.game, channel.title]}\" \
						\"#{Notifications[:text][:body][event] %
							[channel.name, channel.game, channel.title]}\"")
				end
				if Statistics[:enabled] && Statistics[:events][event]
					Statistics[:fields].each_with_index do |field, i|
						if i < Statistics[:fields].length - 1
							P.statFile.print stat[field], Statistics[:separator]
						else
							P.statFile.print stat[field], "\n"
						end
					end
				end
			end
		end
		UpdateMutex.unlock
		raise SuspendedExecutionError if (Time::now - startTime) > Updates[:interval]
	rescue *TwitchAPI::NETWORK_EXCEPTIONS => e
		P.sleep_waiting_for_connection(e)
		retry
	rescue SuspendedExecutionError
		retry
	end

	UpdateMutex.synchronize {
		Printer.print(P.channels.values)
	}
	GC.start
	sleep Updates[:interval] - (Time::now - startTime)
end
end

if Runtime[:enabled]
# ---- Handle signals and other stuff
loop do
	begin
		sleep
		UpdateMutex.synchronize do
			if Runtime[:reload_on_modification][:what].include? :token
				token_valid = P.load_and_check_token(Token)
				if token_valid != :nop && Runtime[:reload_on_modification][:verbose]
					puts
					if token_valid
						puts "Loaded new token, logging in as: #{P.username}"
					else
						puts "Reverting to previous token"
					end
				end
			end
			if Runtime[:reload_on_modification][:what].include? :channels
				new_channels, removed_channels = *P.load_channels(Channels)
				if new_channels != :nop && Runtime[:reload_on_modification][:verbose]
					puts
					if removed_channels && !removed_channels.empty?
						puts "Removed #{removed_channels.length} channel(s):"
						Printer.print_array(removed_channels.map {|ch| ch.display_name})
					end
					unless new_channels.empty?
						puts "Loaded #{new_channels.length} new channel(s):"
						Printer.print_array(new_channels.map {|ch| ch.display_name })
					end
				end
			end
		end
	rescue SignalException => e
		signame = Signal.signame(e.signo)
		if signame == Runtime[:update][:signame]
			# Update immediately
			LoopThread.wakeup
		elsif signame == Runtime[:reload_on_signal][:signame]
			UpdateMutex.synchronize {
				# Reload token
				if Runtime[:reload_on_signal][:what].include? :token
					token_valid = P.load_and_check_token(Token, true)
					if Runtime[:reload_on_signal][:verbose]
						puts
						if token_valid
							puts "Loaded new token, logging in as: #{P.username}"
						else
							puts "Reverting to previous token"
						end
					end
				end
				# Reload channels
				if Runtime[:reload_on_signal][:what].include? :channels
					new_channels, removed_channels = *P.load_channels(Channels, true)
					if Runtime[:reload_on_signal][:verbose]
						puts
						if removed_channels && !removed_channels.empty?
							puts "Removed #{removed_channels.length} channel(s):"
							Printer.print_array(removed_channels.map {|ch| ch.display_name})
						end
						unless new_channels.empty?
							puts "Loaded #{new_channels.length} new channel(s):"
							Printer.print_array(new_channels.map {|ch| ch.display_name })
						end
						if new_channels.empty? and (!removed_channels or removed_channels.empty?)
							puts "No changes in channels"
						end
					end
				end
				LoopThread.wakeup if Runtime[:reload_on_signal][:update]
			}
		else
			raise e
		end
	end
end

# If Runtime stuff isn't enabled, we don't need to do anything but wait for termination
else
	sleep
end
